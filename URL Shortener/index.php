<?php ob_start();?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<title>URL Pendek </title>
<link rel="shortcut icon" href="favicon.png"/>
<!-----common style----->
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<!-----menu style----->
<script type="text/javascript" src="js/jquery-1.11.1.js"></script>

<!-----bootsrap js----->
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!----------------viewport css------------------>
<link rel="stylesheet" type="text/css" href="css/view_port.css"/>

<!--[if lte IE 8]>
        <script src="js/html5.js" type="text/javascript"></script>
        <script src="js/css3-mediaqueries.js" type="text/javascript"></script>
        <script src="js/respond.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="css/ie8-and-down.css" />
        <![endif]-->

</head>

<body class="short_bg">
<div id="short_wrapper">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="short_frame">
          <div class="short">
            <div class="short_inner ">
			<div class="short_filed">
			<?php
			$url_file = 'file_urls.txt';//URLS stored in this file
			$rewrite_or_not = 1;//If the server don't support rewrite change it to "0"
			
			//Variables Initializing Start
			$blank_url		= '<strong> URL Not Entered </strong>';
			$short_url		= '<strong> Short URL:</strong>';
			$not_a_valid	= '<strong>Please Enter a valid URL.</strong>';
			$action			= "";
			$url_exist		=false;
			$line_number = false;
			$id=0;
			//Variables Initializing End

				if(!is_writable($url_file) || !is_readable($url_file))
					{
						echo "Change the file write permissions";
					}
				if(isset($_GET['id'])){
					$action = trim($_GET['id']);
					}
					$action = (empty($action) || $action == '') ? 'insert_url_file' : 'redirect_the_link';
					$output = '';

						
						
				if($action == 'insert_url_file')
					{

						if(isset($_POST['insert_url_file']))
							{
								$url = trim($_POST['url']);
								
								if($url == '')
								{
									$output = $blank_url;
								}
								else
								{
								if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url)) 
									{ //if the entered URL is valid
										$fp = fopen($url_file, 'a+');
										
										$id			= count(file($url_file));
										
										$urls_in_file = file($url_file);
										if($id==0){
											fwrite($fp, "{$url}\r\n");
											$id			= count(file($url_file));
										}
										else{
										  $file_con = file_get_contents($url_file);
										  
										
												if (strstr($file_con, $url)) 
												{
												$url_exist	=	true;
												$search    	= 	$url;
												//Helps to find the id number (Line number in file)
												if ($handle = fopen($url_file, "r")) {
												   $count = 0;
												   while (($line = fgets($handle, 4096)) !== FALSE and !$line_number) {
													  $count++;
													  $line_number = (strpos($line, $search) !== FALSE) ? $count : $line_number;
												   }
												   fclose($handle);
												}
												}else{
												//URL not exist then add it to the List
													fwrite($fp, "{$url}\r\n");
													$id			= count(file($url_file));
												}
										
										}
										
										$dir		= dirname($_SERVER['PHP_SELF']);
										$filename	= explode('/', $_SERVER['PHP_SELF']);
										$filename   = $filename[(count($filename)-1)];
										
										
										if($url_exist){//if URL already exist in list
											$shorturl = ($rewrite_or_not == 1) ? "http://{$_SERVER['HTTP_HOST']}{$dir}/{$line_number}" : "http://{$_SERVER['HTTP_HOST']}{$dir}/{$filename}?id={$line_number}";
											$output = "Entered URL already in our list<br/><a href='{$shorturl}' target='_blank'>{$shorturl}</a>";
										}else{//if URL not exist in list
											$shorturl = ($rewrite_or_not == 1) ? "http://{$_SERVER['HTTP_HOST']}{$dir}/{$id}" : "http://{$_SERVER['HTTP_HOST']}{$dir}/{$filename}?id={$id}";
											$output = "<a href='{$shorturl}' target='_blank'>{$shorturl}</a>";
											fclose($fp);
										}
									}
									else
									{//if the entered URL is not valid
										$output = $not_a_valid;
									}
								}
							}
						}

						if($action == 'redirect_the_link')
						{//open the link using pendek(short) URL
							$urls_in_file = file($url_file);
							$id="";
							if(isset($_GET['id']))
							{
								$id   = trim($_GET['id']) - 1;
							}
							if(isset($urls_in_file[$id]))
							{
							
								header("Location: {$urls_in_file[$id]}");

							}
							else
							{
								echo "Error Occured While Redirect";
							}
						}

			?>
			<h1>Pendek URL</h1>
						<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
						<label>Enter Your Long URL:</label><br/>
							<input type="text" name="url" placeholder="Enter Your Long URL" value="http://" class="short_filed_style"/>
							<input type="submit" class="short_btn" name="insert_url_file" value="Pendek URL"/>
					   </form>
			   </div> 
				<span class="short_btm_txt"> Pendek URL:<p class="response"><?php echo $output;?></p> </span>
			  
              <div class="clearfix"></div>
			  
			<p class="copyright"> &copy; <?php echo date('Y');?> ARUN ARAVIND</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html><?php ob_flush();?>
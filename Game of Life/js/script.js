
		var width_grid = 35;
		var height_grid = 35;
		var loop_grid = 1;
		var steps_block;
		var grid_block;
		var updated_grid_block;
		var changed;

		function window_onload()
		{
		$("#settings_content").hide();
		
		$("#settings").click(function(){
		
		$("#settings_content").slideToggle("slow");
		
		});
			resetGridTable();
		}
		function resetGridTable()
		{
			auto = false;
			steps_block = 0;
			createGrid();
			createGridSpace();
			playStopButton();
		}
		function playStopButton()
		{
			var btn = document.getElementById("button_auto");
			if (auto) {
				btn.innerHTML = "STOP";
				btn.title = "Stop the automatic steps movement";
				$(btn).removeClass("btn-success");
				$(btn).addClass("btn-danger");
			} else {
				$(btn).removeClass("btn-danger");
				$(btn).addClass("btn-success");
				
				btn.innerHTML = "PLAY";
				btn.title = "Start automatic steps movement";
			}
		}
	
		function toggleGrid()
		{
			auto = !(auto);
			playStopButton();
			document.getElementById("reset").disabled = auto;
			document.getElementById("button_step").disabled = auto;
			if (auto) {
				step();
			}
		}
	
		function next()
		{
			auto = false;
			step();
		}
		function gridBlock(e)
		{
			var block;
			try {
				block = e.target;	//getting the event source in Mozilla Firefox
			} catch (e) {
				block = window.event.srcElement;	//getting the event source in MSIE
			}
			if (block.active == "false") {
				block.active = "true";
				block.style.backgroundColor = "black";
				grid_block[block.x][block.y] = true;
			} else {
				block.active = "false";
				block.style.backgroundColor = "white";
				grid_block[block.x][block.y] = false;
			}
		}
		function createGrid()
		{
			grid_block = new Array(width_grid);
			updated_grid_block = new Array(width_grid);
			changed = new Array(width_grid);
			for (var x = 0; x < width_grid; x++) {
				grid_block[x] = new Array(height_grid);
				updated_grid_block[x] = new Array(height_grid);
				changed[x] = new Array(height_grid);
				for (var y = 0; y < height_grid; y++) {
					grid_block[x][y] = false;
					updated_grid_block[x][y] = false;
					changed[x][y] = false;
				}
			}
		}
		function createGridSpace()
		{
			//delete all rows
			var arena = document.getElementById("playarea");
			var number_rows = arena.rows.length;
			for (var y = 0; y < number_rows; y++) {
				arena.deleteRow(-1);
			}

			// add all rows
			for (var y = 0; y < height_grid; y++) {
				var row = arena.insertRow(-1);
				for (var x = 0; x < width_grid; x++) {
					var block = row.insertCell(-1);
					block.className = "block";
					block.innerHTML = "&nbsp;";
					block.x = x;
					block.y = y;
					block.active = "false";
					block.onclick = gridBlock;
				}
			}
			steps_block = 0;
		}
		function step()
		{
			steps_block++;
			makeNewBlock();
			addGridSpace();
			if (auto) {
				setTimeout("step();", 100 * loop_grid);
			}
		}
		function makeNewBlock()
		{
			resetTemps();
			for (var y = 0; y < height_grid; y++) {
				for (var x = 0; x < width_grid; x++) {
					var active_now = grid_block[x][y];
					var active_aft_action = false;
					var num_nearest = numOfNearBlocks(x, y);

					if ((active_now) && (num_nearest < 2))
					{
						active_aft_action = false;
					}
					else if ((active_now) && (num_nearest > 3))
					{
						active_aft_action = false;
					}
					else if ((!active_now) && (num_nearest == 3))
					{
						active_aft_action = true;
					}
					else if ((active_now) && ((num_nearest == 2) || (num_nearest == 3)))
					{
						active_aft_action = true;
					}
					updated_grid_block[x][y] = active_aft_action;
					changed[x][y] = (active_now != active_aft_action);
				}
			}
			updategrid_block();
		}
		function updategrid_block()
		{
			for (var x = 0; x < width_grid; x++) {
				for (var y = 0; y < height_grid; y++) {
					grid_block[x][y] = updated_grid_block[x][y];
				}
			}
		}
		function resetTemps()
		{
			for (var x = 0; x < width_grid; x++) {
				for (var y = 0; y < height_grid; y++) {
					updated_grid_block[x][y] = false;
					changed[x][y] = false;
				}
			}
		}
		function addGridSpace()
		{
			var grid_area = document.getElementById("playarea");
			for (var y = 0; y < height_grid; y++) {
				var cells = grid_area.rows[y].cells;
				for (var x = 0; x < width_grid; x++) {
					if (changed[x][y]) {
						cells[x].style.backgroundColor = (grid_block[x][y]) ? "black" : "white";
					}
				}
			}
		}
		function numOfNearBlocks(x, y)
		{
			var n = 0;
			if (blockActive(x-1, y-1))	n++;
			if (blockActive(x-1, y))	n++;
			if (blockActive(x-1, y+1))	n++;
			if (blockActive(x, y-1))	n++;
			if (blockActive(x, y+1))	n++;
			if (blockActive(x+1, y-1))	n++;
			if (blockActive(x+1, y))	n++;
			if (blockActive(x+1, y+1))	n++;
			return n;
		}
		function blockActive(x, y)
		{
			var active = false;
			if ((x >= 0) && (x < width_grid) && (y >= 0) && (y < height_grid)) {
				active = grid_block[x][y];
			} else {
				active = false;
			}
			return active;
		}